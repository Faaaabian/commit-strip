import ctypes
import os
import urllib.request
import urllib3
import datetime
from PIL import Image, ImageOps
from bs4 import BeautifulSoup
import inspect

save_name = 'wallpaper.jpg'

src = None
http = urllib3.PoolManager()

def whoami():
    return inspect.stack()[1][3]

def get_image_link():
    try:
        r = http.request("GET", "https://www.commitstrip.com/en/?random=1")
        soup = BeautifulSoup(r.data, 'html.parser')

        images = soup.findAll('img')
        for image in images:
            src = image['src']
            if "commitstrip" in src:
                return src
    except:
        error(whoami())


def download_image(image_url):
    try:
        save = urllib.request.urlretrieve(image_url, save_name)
    except:
        error(whoami())

def format_image():
    try:
        img = Image.open(save_name)
        size = (1920, 1080)
        img = ImageOps.fit(img, (img.size[0] * 3, img.size[1] * 3), Image.ANTIALIAS)
        img.thumbnail(size)
        img.save(save_name)
    except:
        error(whoami())


def set_image_as_background(path):
    try:
        SPI_SETDESKWALLPAPER = 20
        ctypes.windll.user32.SystemParametersInfoW(SPI_SETDESKWALLPAPER, 0, path, 0)
    except:
        error(whoami())

def error(function):
        print("[{}] Oops, an Error occured in {}! Try running this script again!".format(datetime.datetime.now(), function))
        exit()


if __name__ == '__main__':
    src = get_image_link()
    download_image(src)
    format_image()
    set_image_as_background("{}\\{}".format(
        os.path.abspath(os.getcwd()),
        save_name
    ))
    print("[{}] Successfully changed wallpaper!".format(datetime.datetime.now()))

